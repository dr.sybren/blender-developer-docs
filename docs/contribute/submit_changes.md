# Submit Changes

TODO: TODO

## Pushing Changes

**Checkout with write access**

The repository must be accessed with the `git@` protocol instead `https://` to
work over SSH. If there's already checkout, it is possible to switch it to the
SSH protocol (no re-download needed):
```sh
git remote set-url origin git@projects.blender.org:blender/blender-developer-docs.git
```

**SSH Keys**

Follow the [usual
instructions](https://developer.blender.org/docs/handbook/contributing/using_git/#ssh-keys)
for setting up SSH keys if you haven't done this yet (e.g. for pushing to the
official Blender repository).


## Create a Pull Request
