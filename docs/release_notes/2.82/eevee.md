# Blender 2.82: EEVEE

## Viewport Render Passes

In the 3d viewport an option was added to preview an EEVEE render pass.
Render passes that are supported are

- Combined
- Ambient Occlusion
- Normal
- Mist
- Subsurface Direct
- Subsurface Color

In the future more render passes will be added.

## Volumetric with partial transparency fix

A bug was fixed that changes how Alpha Blended surfaces reacts to
volumetric effect. The old behavior was adding more light if the surface
was partially transparent.

|Before|After|
|-|-|
|![](../../images/EEVEE_282_volume_fix_before.png){style="width:240px;"}|![](../../images/EEVEE_282_volume_fix_after.png){style="width:300px;"}|

This fix will alter the appearance of old file, there is no workaround
other than to tweak the affected materials.

(blender/blender@39d5d11e022a)

## Improvements

- Group node sockets now converts the values as in Cycles.
  (blender/blender@c2e21b23296)
- Curve, surface and text object can now use normal mapping without
  workaround.
  (blender/blender@15350c70be8
  blender/blender@c27d30f3eaf
  blender/blender@bcacf47cbcf)
- Disk lights artifact appearing on certain drivers have been fixed.
  (blender/blender@3d73609832d)
- Volumetric objects with zero volume do not fill the scene anymore.
  (blender/blender@0366c46ec64)
- "Bake Cubemap Only" operator does not reset the Irradiance Volume
  after file load or if world is updated.
  (blender/blender@5d5add5de2b)
- Improved quality of render passes with high samples count
  (blender/blender@17b63db4e272)
- Improved performance when calculating render passes
  (blender/blender@9d7f65630b20)
- Improved performance during viewport rendering
  (blender/blender@7959dcd4f631)
