# Blender 2.93: More Features

#### `--open-last` CLI argument

The new commandline argument `--open-last` opens the most recent file
(blender/blender@3649b5b6dfff).

These two commands behave exactly the same:

``` shell
blender --open-last
blender (first line of $BLENDCONFIG/2.93/config/recent-files.txt)
```

#### `--log` CLI argument

The wild card `\*` can now be used to match a random substring.

E.g. `blender --log "\*undo\*"` will match log messages from any undo
sources (`edit`, `bke`, etc.).
