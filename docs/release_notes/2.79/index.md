# Blender 2.79 Release Notes

![](../../images/Splash_279.png){style="width:400px;"}

**The Blender Foundation and online developer community are proud to
present Blender 2.79!**

## [**Download the 2.79 Release**](https://www.blender.org/download/)

In this release:

- Cycles: Built-in Denoising, Shadow catcher, Principled shader, AMD
  OpenCL optimizations.
- Grease Pencil: New frame interpolation tools, per-layer onion
  skinning.
- Alembic: Improvements to compatibility, stability and support.
- User Interface: Initial support for reusable custom configurations,
  automatic DPI scaling.
- Twenty Three new and several updated add-ons.
- **And**: 100s of bug fixes and other improvements!

## [**2.79a Corrective Release**](a.md)

This corrective release does not add any new feature. It brings a huge
amount of fixes (over 200, from real bugs to potential security issues).
And some performance enhancements, mostly in threaded code (e.g. in mesh
normal and tangent space computation, in SubSurf modifier).

It aims at making Blender 2.79 even more robust and reliable while most
of the development effort is focused on the next version, Blender 2.8.

## [**2.79b Corrective Release**](b.md)

This corrective release only fixes a few serious bugs/regressions
introduced in 2.79a fixes.

  
----

## Compatibility Warning

- Due to various issues with drivers for AMD GCN 1.0 architecture, those
  cards are no longer supported for Cycles rendering
- When using add-ons in Blender 2.79 that take advantage of the new
  [data-block pointer
  properties](pythonapi.md), the
  resulting .blend files can't be opened in earlier versions of Blender.
  Attempting to open such .blend files in Blender 2.78c and earlier may
  crash.
- Some Interface Themes may need to be reloaded to work properly.
- The text color contained in a *layout.box()* is now defined in *User
  Preferences \>\> Themes \>\> User Interface \>\> Box* section
- Rig compatibility:
  - Changes to bbone easing break forward compatibility (i.e. rigs using
    this feature saved in 2.79 won't behave correctly when loaded in
    older versions, see [D2796](http://developer.blender.org/D2796)).
  - Changes to IK snapping may affect rigs that use non-uniform IK
    scaling. (rigs created in the 2.78 builds.)
- During the 2.78 series: IK snapping was partly broken.
  - This has been fixed for Blender 2.79
  - Rigs created in Blender 2.78 may not be compatible with 2.79.
  - Rigs created in Blender 2.77 may still work.
- When using the add-on Rigify, please note:
  - Compatibility is broken for this release. There's no guarantee rigs
    created in previous Blender versions will work correctly.
  - Rigs created in Blender 2.78 may not be compatible with 2.79.
  - Rigs created in Blender 2.77 may still work.
  - Save your work before attempting upgrading your rigs.

  
----

## [**Cycles Rendering**](cycles.md)

![](../../images/Cycles2.79_denoise_room_after.jpg){style="width:300px;"}

- Denoising built into the renderer.
- Principled BSDF shader to render a wide variety of materials.
- Shadow catcher to composite CGI elements in real-life footage.
- Faster AMD OpenCL rendering and feature parity with NVidia CUDA.
- Filmic color management for better handling of high dynamic range.

  

## [**User Interface**](ui.md)

- Application templates to define a reusable configuration.
- Automatic scaling for high DPI displays on Windows on Linux.
- Reorganized sequencer and UV editor panels.
- Custom shortcuts for keyframing and drivers.

  

## [**Grease Pencil**](gpencil.md)

![](../../images/Blender2.79_grease_pencil.jpg){style="width:300px;"}

- New tools for interpolating between grease pencil frames.
- Per-layer onion skinning, add blank frame tool, and UI improvements.

  

## [**Alembic**](alembic.md)

![](../../images/Blender2.79_alembic_logo.jpg){style="width:300px;"}

- Alembic import and export has been greatly improved, both in
  compatibility and stability.
- New supported features include export of linked dupli-groups,
  sub-frame sampling, face-varying vertex colors, child hairs and
  empties.

  

## [**Mesh Modeling**](modelling.md)

![](../../images/Blender_Surface_Deform_Modifier.jpg){style="width:300px;"}

- New surface deform modifier, to transfer motion from another mesh.
- Improved displace and mirror modifiers.
- Various small new tools and options.

  

## [**Animation**](animation.md)

![](../../images/Blender2.79-dopehseet_channel_colors.png){style="width:300px;"}

- Pose library reordering and keying for selected bones only.
- Better undo for frame changes.
- Various small new tools and options.

  

## [**More Features**](more_features.md)

![](../../images/Blender2.78_viewport_object_info.jpg){style="width:300px;"}

- Video encoding settings have been simplified, along with the addition
  of a Constant Rate Factor (CRF) mode.
- Blender render and GLSL viewport support for object info, layer weight
  and Fresnel shading nodes.

  

## [**Python API**](pythonapi.md)

![](../../images/Blender_Python_properties_illustration.jpg){style="width:300px;"}

- Custom properties pointing to data-blocks like objects or materials.
- Render engine add-ons can now add custom render passes.
- Inserting custom items in right click menus.

  

## [**Add-ons**](add_ons.md)

![](../../images/Tissue_v02_cover.jpg){style="width:300px;"}

- **New**: Dynamic Sky, Archipack, Magic UV, Mesh Edit Tools, Skinify,
  Display Tools, Brush Menus, Btrace, Is Key Free, Turnaround Camera,
  Auto Mirror, Camera Rigs, Snap Utils Line, Add Advanced Objects,
  Export Paper Model, Kinoraw Tools, Stored Views, Render Clay, Auto
  Tracker, Refine Tracking Solution, Materials Library VX, Mesh Tissue,
  Cell Fracture Crack It.
- **Updated**: Collada, POV-Ray, OBJ, Rigify, Ant Landscape, Add Curve
  Extra Objects, Viewport Pie Menus, Blender ID, Node Wrangler.
- Various fixes and improvements across the board.

  

## [**Bug Fixes**](bug_fixes.md)

- As for every Blender release, hundreds of bugs were fixed, thanks to
  the hardworking Blender developers.

  
