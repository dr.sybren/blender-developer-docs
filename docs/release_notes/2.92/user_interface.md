# Blender 2.92: User Interface

## Outliner

- Properties editor sync. Clicking on the icon for outliner data will
  switch to the corresponding properties editor tab for properties
  editors that share a border.
  (blender/blender@0e47e57eb77d)
- New filter to display only selectable objects.
  (blender/blender@fbf29086745f)
- The object state filter can now be inverted. For example, inverting
  "Selectable" will only display unselectable objects.
  (blender/blender@2afdb4ba875)
- List library overrides in the Outliner. A filter option allows hiding
  them
  (blender/blender@aa64fd69e733).

## Properties Editor

- Add a new tab for collection properties
  (blender/blender@3e87d8a4315d).

## 3D Viewport

- Use consistent name for select mirror across object modes
  (blender/blender@1c357a3c5fc1).
- Cycling object selection is now ordered by depth (nearest first)
  (blender/blender@3e5e204c81a2aa136ad645155494b7ab132db2bf).

## General Changes

- Support tooltips for superimposed icons
  (blender/blender@2250b5cefee7).
- Improvements to the alignment and placement of interface items
  (blender/blender@6bf043ac946f)
  (blender/blender@b0a9a04f6201).
- Allow theming the alternate row color in the sequencer, similar to the
  file browser and the outliner
  (blender/blender@f7223d5f722ac4).
- When dragging, constrain panels vertically in the property editor,
  preferences, and side-bars vertically
  (blender/blender@328aad8c98c931c3).
- When using Weight Paint Sample Weight Tool, using eyedropper cursor
  and display weight in header
  (blender/blender@64277e8f3a7e)
  (blender/blender@e3d9241a0532).
- New layout for the "About" dialog with full Blender logo
  (blender/blender@79eeabafb39f).

![](../../images/About_Dialog.png){style="width:600px;"}

- New look for the "Startup Script Execution" warning dialog
  (blender/blender@2e5d9a73f7b9).

![](../../images/Security-alert-dialog-box.png){style="width:600px;"}

- Improved layout of node group socket lists
  (blender/blender@1d3b92bdeabc).
- New properties editor popover that contains settings for outliner to
  properties sync
  (blender/blender@ffacce5be41)
