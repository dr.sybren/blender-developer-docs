# Blender 2.80 Release Notes

## [User Interface](ui.md)

Blender has a revamped user interface, to make it easier to discover and
use its many features.

## [Tools & Gizmos](tools.md)

The 3D viewport and UV editor have new interactive tools and gizmos,
along with a new toolbar.

## [Eevee](eevee.md)

Eevee is a new physically based realtime renderer. It works both as a
renderer for final frames, and as the engine driving Blender's realtime
viewport for creating assets.

## [Viewport](viewport.md)

The 3D viewport was completely rewritten, to optimize it for modern
graphics cards and add powerful new features.

## [Grease Pencil](grease_pencil.md)

A powerful new 2D animation system was added, with a native 2D grease
pencil object type, modifier and shader effects.

## [Cycles](cycles.md)

New volume and hair shaders, bevel and ambient occlusion shaders, along
with many other improvements and optimizations.

## [Collections](layers.md)

The limited layer system has been replaced by collections, a more
powerful way to organize and link objects.

## [Modeling](modeling.md)

Multiple meshes can now be edited together in edit mode, along with
improvements for custom normal editing, bevel and OpenSubdiv.

## [Animation & Rigging](animation.md)

Many new constraint, modifier and bendy bone features were added for
rigging. There is also new keyframe visualization, and improved NLA
editor and easier driver editing.

## [Import / Export](import_export.md)

The Khronos glTF 2.0 importer and exporter is now bundled with Blender.
There are improvements to COLLADA, Alembic and video import and export.

## [Dependency Graph](depsgraph.md)

The core modifier and animation evaluation system was rewritten, to
improve performance and pave the way for new features.

## [More Features](more_features.md)

Improved unit system, cloth physics, background images and video editing
cache system.

## [Removed Features](removed_features.md)

A number of features that were no longer under active development or did
not fit in the new design were removed.

## [Python API](python_api/index.md)

Blender 2.80 is an API breaking release. Addons and scripts will need to
be updated, both to handle the new features and adapt to changes to make
the API more consistent and reliable.

## [Add-ons](add_ons.md)

Most add-ons for 2.79 are available for 2.80 as well. A few new ones
were added, and a few were removed.

  
