# Blender 4.2: VFX & Video

## Compositor

### Node execution time

Added an overlay to display each node's last execution time (blender/blender@467a132166)

The display of execution time is disabled by default, and can be enabled from
the Overlays pop-over. The execution time statistics is gathered during
compositor evaluation in the nodes editor.

NOTE: Currently is only supported by the new compositor implementation which is
currently only available as an experimental option, but will become official as
soon as possible.