# Geometry Node Tests

## Adding tests for Geometry Nodes

Geometry Nodes tests are located under
`lib/tests/modeling/geometry_nodes`

- Open Blender.
- Go to Geometry Nodes Tab.
- Add a new Node Tree.
- Add the node to be tested.
- Rename the default object to `test_object`.
- Save the blend file under any existing category (mesh, points, curves
  ...)
- If the category doesn't exist, add a new folder with the
  <category_name>.

### Running the test

#### Test under an existing category

- Compile blender.
- From the terminal, go to the build folder.
- Run `cmake ./`.
- When running the test for the first time, set the
  `BLENDER_TEST_UPDATE` flag.
- Run test using `ctest`.

#### Test from a new category

- Update the `CMakelists.txt` located in `tests\python` to add the
  new category (Search `geo_node` in the file).
- The rest of the steps are same as above(Tests under Existing
  category).

#### More Resources:

Check out the [Tests section](index.md) - Setup, Automated
Tests and Python Tests.
