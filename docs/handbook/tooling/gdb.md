# Debugging with GDB

A debugger can be used to inspect the state of an application in the event of a crash.

### Compile Debug Build

How to make a debug build depends on the build system used:

- For Linux/macOS set: `CMAKE_BUILD_TYPE=Debug` in `CMakeCache.txt`
- For Visual Studio, set the Release Configuration to Debug

### Run GDB

Start GDB by changing the working directory to the location of your new
debug build and typing one of the following, depending on the platform:

``` bash
gdb blender.exe
```

``` bash
gdb ./blender
```

Then to start Blender, type:

``` bash
run
```

Now make Blender crash. Blender will freeze, so switch to the GDB prompt.
Get a backtrace by typing:

``` bash
bt
```

A more complete backtrace can be printed using:

``` bash
thread apply all bt full
```

For more information, see this guide:
[How to Get a Backtrace](http://wiki.debian.org/HowToGetABacktrace).

### Run Immediately

Blender can be made to run immediately:

``` bash
gdb ./blender --ex=run --args ./blender
```

### Run With Environment Variables Set

Blender can be run with environment variables using `env`.

These options disable some ASAN checks can interfere with debugging:

``` bash
gdb ./blender --ex=run --args env ASAN_OPTIONS=check_initialization_order=0:leak_check_at_exit=0 ./blender
```

