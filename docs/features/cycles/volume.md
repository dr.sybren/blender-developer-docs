# Cycles Volume Render

### To Do

- Support BSDFs in volumes
  - Material density output (like displacement)
  - Gradient of density field = normal, compute like bump mapping

<!-- -->

- Multiple scattering approximation with octaves
- Use ray differentials for volume shading

<!-- -->

- Verify lamp MIS correctness with volumes and mix of surfaces and
  volumes.

<!-- -->

- World volumes
  - Do we need an option to exclude the world volume from the interior
    of objects? Some more general mechanism?
  - Very low density should not be optimized out for closures, now there
    is a discontinuity.

<!-- -->

- Incorrectly connected surface shader to volume socket: detect and
  ignore? ([\#39089](http://developer.blender.org/T39089))

<!-- -->

- Random jitter offset for heterogeneous volumes can use stratified
  samples
- Add a user setting for transparency cutoff, as optimization and to
  make creating opaque volumes possible without infinite density.
- Ray visibility (partially there)
