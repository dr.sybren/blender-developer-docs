---
hide:
  - toc
---

# Blender Human Interface Guidelines

The Blender Human Interface Guidelines (HIG) aim to provide a clear path
towards understanding Blender's user interaction schemes and how they
apply to its variety of workflows.  
They should be a practical resource for contributors, including Add-on
authors, designers and core developers.

Please respect that the HIG is rather extensive and that we can't
reasonably expect contributors to know all of it.

!!! Warning "Work in Progress"
    The Blender Human Interface Guidelines are still in development. They
    are unpolished and a lot of content is missing still.

!!! Note Proposal for restructuring
    The current page structure is temporary, see
    [T97989](http://developer.blender.org/T97989).

## TODO

Here are the things we should address before sharing the HIG more
widely:

- Initial vision statement.
- Add more contents to
  [Layouts](layouts.md) page.
- Add/improve contents on
  [Process](process.md) page.
- Update [Sidebar
  Tabs](sidebar_tabs.md) for 2.8
  changes.
- Clearly mark which pages are "approved".
- Make things prettier :) (mostly through pictures).
